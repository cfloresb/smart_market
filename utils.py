__author__ = 'camilo'

def validar_rut(rut, dv):
    """
    Función adaptada de https://es.wikipedia.org/wiki/Anexo:Implementaciones_para_algoritmo_de_rut#Python
    """
    value = 11 - sum([int(a)*int(b) for a, b in zip(str(rut).zfill(8), '32765432')]) % 11
    dv_calculado = {10: 'K', 11: '0'}.get(value, str(value))
    if dv != dv_calculado:
        return False
    return True

def validar_email(email):
    """
    Sencilla función para calcular si un string es más menos similar a una dirección de correo.
    En realidad se debería utilizar algo más robusto, ocupando Regular Expressions
    """
    if '@' not in email:
        return False
    email_separado = email.split('@')
    if len(email_separado) != 2:
        return False
    if '.' not in email_separado[1]:
        return False
    dominio = email_separado[1].split('.')
    if len(dominio[1]) < 2:
        return False

    return True

