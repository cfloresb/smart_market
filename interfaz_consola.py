from modelos import Compra, Venta, Existencia, Inventario, Persona, Producto, ProductoLimpieza, ProductoPerecible

import pickle

def imprimir_menu(nombre_menú, opciones):
    while True:
        try:
            print('\n#### {} ###'.format(nombre_menú))

            for indice, opción in enumerate(opciones):
                print(indice + 1, ":", opción[0])

            selección = int(input("\nIngrese su opción: --> "))
            if selección < 1 or selección > len(opciones):
                raise ValueError("Indice no válido")
            if selección == len(opciones):
                break

            opciones[selección-1][1]()
        except ValueError:
            print("\n ** Selección no válida. Por favor ingrese un número dentro de las opciones ofrecidas. ** \n")


def revisar_productos():
    imprimir_menu('Menú productos', menu_productos)

def revisar_clientes():
    imprimir_menu('Menú clientes', menu_clientes)

def revisar_inventario():
    imprimir_menu('Menú inventario', menu_inventario)

def ingresar_compra():
    nueva_compra = Compra()
    while True:
        try:
            print(" *** Ingreso de compra ** ")
            print(" 1 : Ingresar un nuevo stock de producto")
            print(" 2 : Finalizar compra")
            opcion = int(input("Ingrese opcion -->"))
            if opcion == 1:
                while True:
                    try:
                        indice = int(input("Ingrese el índice del producto a registrar: "))
                        precio_compra = int(input("Ingrese el precio de compra unitario: "))
                        cantidad = int(input("Ingrese la cantidad de unidades a registrar: "))
                        for existencia in range(0, cantidad):
                            existencia = Existencia(base_de_datos['productos'][indice-1], precio_compra, 0)
                            nueva_compra.agregar_existencia(existencia)
                        break
                    except LookupError:
                        print("Indice de producto no válido. Intente nuevamente.")
            elif opcion == 2:
                base_de_datos['compras'].append(nueva_compra)
                for nombre_producto, existencias in nueva_compra.lista_stocks.items():
                    for existencia in existencias:
                        base_de_datos['inventario'].agregar_existencia(existencia)
                print("** Compra ingresada exitosamente **")
                return
            else:
                print("Ingrese una opción válida")
        except ValueError:
            print("\n ** Selección no válida. Por favor ingrese un número dentro de las opciones ofrecidas. ** \n")


def ingresar_venta():
    nueva_venta = Venta()
    while True:
        try:
            print(" *** Ingreso de venta ** ")
            print(" 1 : Ingresar un nuevo producto")
            print(" 2 : Finalizar venta")
            opcion = int(input("Ingrese opcion -->"))
            if opcion == 1:
                while True:
                    try:
                        indice = int(input("Ingrese el índice del producto a vender: "))
                        precio_venta = base_de_datos['productos'][indice-1].precio_venta
                        cantidad = int(input("Ingrese la cantidad de unidades a vender: "))
                        nombre_producto = base_de_datos['productos'][indice-1].nombre
                        for existencia in range(0, cantidad):
                            nueva_venta.agregar_existencia(base_de_datos['inventario'].retirar_existencia(nombre_producto))
                        break
                    except LookupError:
                        print("Indice de producto no válido. Intente nuevamente.")
            elif opcion == 2:
                base_de_datos['ventas'].append(nueva_venta)
                print("** Venta ingresada exitosamente **")
                print("\n *** El total a pagar es: ", nueva_venta.valor_total, " ***")
                return
            else:
                print("Ingrese una opción válida")
        except ValueError:
            print("\n ** Selección no válida. Por favor ingrese un número dentro de las opciones ofrecidas. ** \n")

def calcular_balance():
    total_compras = 0
    for compra in base_de_datos['compras']:
        total_compras += compra.valor_total

    total_ventas = 0
    for venta in base_de_datos['ventas']:
        total_ventas += venta.valor_total

    print("Total de compras: ", total_compras)
    print("Total de ventas: ", total_ventas)
    print("______________________________")
    print("Balance final:   ", total_ventas-total_compras)


def guardar_información():
    nombre_archivo = input("Ingrese el nombre de archivo donde almacenar la información: ")
    try:
        archivo = open(nombre_archivo, 'r')
        if archivo:
            opcion = input("Un archivo con ese nombre ya existe. ¿Desea sobreescribirlo? (s/N)")
            if opcion.lower() != 's':
                return

    except FileNotFoundError:
        archivo = open(nombre_archivo, 'wb')
        pickle.dump(base_de_datos, archivo)
        archivo.close()
        print("Información exitosamente guardada.")

def cargar_información():
    nombre_archivo = input("Ingrese el nombre de archivo desde donde cargar la información: ")
    try:
        archivo = open(nombre_archivo, 'rb')
        # FIXME: "mezclar" la información existente en vez de sobreescribirla
        opcion = input("Esto sobreescribirá toda la información ingresada en la aplicación. ¿Desea continuar? (s/N): ")
        if opcion.lower() != 's':
            return

        global base_de_datos
        base_de_datos = pickle.load(archivo)
        archivo.close()
    except FileNotFoundError:
        print("Nombre de archivo no válido.")


def listar_elementos(nombre_lista):
    lista = base_de_datos[nombre_lista]
    if len(lista) == 0:
        print("No hay", nombre_lista, "registrados actualmente.")
    else:
        for indice, elemento in enumerate(lista):
            print("[", indice+1, "] - ", elemento)


def modificar_elemento(nombre_lista, funcion_creacion):
    lista = base_de_datos[nombre_lista]
    while True:
        try:
            nombre_elemento = nombre_lista[:-1]
            indice = int(input("Ingrese el índice del " + nombre_elemento +" a modificar: -->"))
            elemento_a_modificar = lista[indice-1]
            print("Se modificará el siguiente", nombre_elemento, ":")
            elemento_a_modificar.imprimir()
            print("\n Ingrese la información solicitada: \n")

            nuevo_elemento = funcion_creacion()
            lista[indice-1] = nuevo_elemento
            return

        except LookupError:
            print("Índice no válido. Por favor intente nuevamente.")
        except ValueError:
            print("Ingrese un valor numérico por favor.")


def eliminar_elemento(nombre_lista):
    lista = base_de_datos[nombre_lista]
    while True:
        nombre_elemento = nombre_lista[:-1]
        indice = int(input("Ingrese el índice del " + nombre_elemento + " a eliminar: -->"))
        try:
            lista.remove(lista[indice-1])
            return
        except LookupError:
            print("Índice no válido. Por favor intente nuevamente.")
        except ValueError:
            print("Ingrese un valor numérico por favor.")

def listar_productos():
    listar_elementos('productos')


def crear_producto():
    while True:
        try:
            nombre = input("Ingrese el nombre: ")
            marca = input("Ingrese la marca: ")
            precio_venta = int(input("Ingrese el precio de venta: "))
            return Producto(nombre, marca, precio_venta)
        except ValueError:
            print("Ingrese un precio numérico. Inténtelo nuevamente.")


def ingresar_producto():
    base_de_datos['productos'].append(crear_producto())


def modificar_producto():
    modificar_elemento('productos', crear_producto)


def eliminar_producto():
    eliminar_elemento('productos')


def listar_clientes():
    listar_elementos('clientes')


def crear_cliente():
    while True:
        try:
            nombres = input("Ingrese los nombres: ")
            apellidos = input("Ingrese los apellidos: ")
            rut = input("Ingrese el rut: ")
            email = input("Ingrese el correo electrónico: ")
            dirección = input("Ingrese la dirección: ")
            return Persona(nombres, apellidos, rut, email, dirección)

        except ValueError as error:
            print("Error al crear el cliente. Detalle: ", error)
            print("Por favor intente nuevamente.")


def ingresar_cliente():
    base_de_datos['clientes'].append(crear_cliente())


def modificar_cliente():
    modificar_elemento('clientes', crear_cliente)


def eliminar_cliente():
    eliminar_elemento('clientes')

menu_principal = [('Revisar B.D productos', revisar_productos),
                  ('Revisar B.D clientes', revisar_clientes),
                  ('Revisar inventario', revisar_inventario),
                  ('Ingresar compra', ingresar_compra),
                  ('Ingresar venta', ingresar_venta),
                  ('Calcular balance', calcular_balance),
                  ('Guardar información', guardar_información),
                  ('Cargar información', cargar_información),
                  ('Salir', None)]

menu_productos = [('Listar productos registrados', listar_productos),
                  ('Ingresar producto', ingresar_producto),
                  ('Modificar producto', modificar_producto),
                  ('Eliminar producto', eliminar_producto),
                  ('Volver al menú principal', None)]

menu_clientes = [('Listar clientes registrados', listar_clientes),
                 ('Ingresar cliente', ingresar_cliente),
                 ('Modificar cliente', modificar_cliente),
                 ('Eliminar cliente', eliminar_cliente),
                 ('Volver al menú principal', None)]

menu_inventario = [('Listar todo el inventario', None),
                   ('Listar productos faltantes', None),
                   ('Volver al menú principal', None)]

base_de_datos = {}
base_de_datos['productos'] = []
base_de_datos['clientes'] = []
base_de_datos['inventario'] = Inventario()
base_de_datos['compras'] = []
base_de_datos['ventas'] = []


imprimir_menu('Menú principal', menu_principal)