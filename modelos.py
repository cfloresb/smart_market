__author__ = 'camilo'
from datetime import datetime

from utils import validar_rut, validar_email

class ObjetoImprimible:
    def imprimir(self):
        for nombre_atributo, valor in vars(self).items():
            print(nombre_atributo.capitalize(), ":", valor)

class Persona(ObjetoImprimible):
    def __init__(self, nombres, apellidos, rut, email, direccion):
        self.nombres = nombres
        self.apellidos = apellidos
        self.direccion = direccion
        self.fecha_creacion = datetime.now()

        rut_dividido = rut.split('-')
        if not validar_rut(rut_dividido[0], rut_dividido[1]):
            raise ValueError("RUT no válido")

        self.rut = rut

        if not validar_email(email):
            raise ValueError("Formato de correo electrónico no válido")
        self.email = email

    def __str__(self):
        return "[" + self.rut + " - " + self.nombres + " " + self.apellidos + "]"


class Producto(ObjetoImprimible):
    def __init__(self, nombre, marca, precio_venta):
        self.nombre = nombre
        self.marca = marca
        self.precio_venta = precio_venta

    def __str__(self):
        return "[Nombre: " + self.nombre + ", Marca: " + self.marca + "]"


class ProductoPerecible(Producto):
    def __init__(self, nombre, marca, fecha_elaboracion, fecha_expiracion):
        super().__init__(nombre, marca)
        self.fecha_elaboracion = datetime.strptime(fecha_elaboracion, '%Y/%m/%d')
        self.fecha_expiracion = datetime.strptime(fecha_expiracion, '%Y/%m/%d')


class ProductoLimpieza(Producto):
    def __init__(self, nombre, marca, es_toxico):
        super().__init__(nombre, marca)
        self.es_toxico = es_toxico


class Existencia(ObjetoImprimible):
    def __init__(self, producto, precio_compra, precio_venta):
        self.producto = producto
        self.precio_compra = precio_compra
        self.precio_venta = precio_venta

    def __str__(self):
        return "[Existencia: " + self.producto.nombre + " $Compra: " + self.precio_compra + " $Venta: " + self.precio_venta + "]"


class Inventario:
    def __init__(self):
        self.stocks = {}

    def agregar_existencia(self, existencia):
        if self.stocks.get(existencia.producto.nombre) is None:
            self.stocks[existencia.producto.nombre] = []

        self.stocks[existencia.producto.nombre].append(existencia)

    def retirar_existencia(self, nombre_producto):
        return self.stocks[nombre_producto].pop()

    def contar_existencias(self, nombre_producto):
        return len(self.stocks.get(nombre_producto))

    def valorizar(self):
        total = 0
        for stock in self.stocks:
            for existencia in stock:
                total += existencia.precio_compra

        return total


class Transacción:
    def __init__(self):
        self.lista_stocks = {}
        self.fecha_creacion = datetime.now()

    def agregar_existencia(self, existencia):
        if self.lista_stocks.get(existencia.producto.nombre) is None:
            self.lista_stocks[existencia.producto.nombre] = []

        self.lista_stocks[existencia.producto.nombre].append(existencia)


class Venta(Transacción):
    def __init__(self, cliente=None):
        super().__init__()
        self.cliente = cliente

    @property
    def valor_total(self):
        total = 0
        for nombre_producto, existencias in self.lista_stocks.items():
            for existencia in existencias:
                existencia.precio_venta = existencia.producto.precio_venta
                total += existencia.precio_venta
        return total


class Compra(Transacción):
    def __init__(self, proveedor=None):
        super().__init__()
        self.proveedor = proveedor

    @property
    def valor_total(self):
        total = 0
        for nombre_producto, existencias in self.lista_stocks.items():
            for existencia in existencias:
                total += existencia.precio_compra
        return total
